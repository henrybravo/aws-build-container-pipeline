# aws-build-container-pipeline

This repo hosts a CloudFormation template to build a CD pipeline to build and deploy Docker containers using AWS: CodePipeline, CodeCommit, CodeBuild and ECR. This example builds a Java container using a prebuild application JAR.
