# Pull java Base Image from AWS public Docker repository
FROM public.ecr.aws/w9q2h2q2/open-jdk-8:latest

# Declare app source to include in image
ARG JAR_FILE=java-app-0.0.1-SNAPSHOT.jar

# Include AWS Batch Job Source
ADD ${JAR_FILE} app.jar

# Do not execute as root
RUN adduser --system --home /var/cache/bootapp --shell /sbin/nologin springboot

# run as non-root user
USER springboot

# java 11 options
ENV _JAVA_OPTIONS "-XX:MaxRAMPercentage=90 -Djava.security.egd=file:/dev/./urandom -Djava.awt.headless=true -Dfile.encoding=UTF-8"

# Fall back to defaults if parameters not set
ENV PARAM1=default
ENV PARAM2=default

ENTRYPOINT ["sh","-c","java -Djob.id=$PARAM1 -Dspring.profiles.active=$PARAM2 -jar /app.jar"]